# TP4 : Vers un réseau d'entreprise

- [TP4 : Vers un réseau d'entreprise](#tp4--vers-un-réseau-dentreprise)
- [I. Dumb switch](#i-dumb-switch)
- [3. Setup topologie 1](#3-setup-topologie-1)
- [II. VLAN](#ii-vlan)
  - [3. Setup topologie 2](#3-setup-topologie-2)
- [III. Routing](#iii-routing)
- [IV. NAT](#iv-nat)
  - [3. Setup topologie 4](#3-setup-topologie-4)
- [V. Add a building](#v-add-a-building)

# I. Dumb switch

# 3. Setup topologie 1

On modifie les ip des 2 VPCS avec la commande `ip 10.1.1.x/24`
```
PC1> ip 10.1.1.1/24
Checking for duplicate address...
PC1 : 10.1.1.1 255.255.255.0
```

```
PC2> ip 10.1.1.2/24
Checking for duplicate address...
PC2 : 10.1.1.2 255.255.255.0
```
On ping ensuite l'autre machine pour voir si elle sont connectées
```
PC2> ping 10.1.1.1

84 bytes from 10.1.1.1 icmp_seq=1 ttl=64 time=8.914 ms
84 bytes from 10.1.1.1 icmp_seq=2 ttl=64 time=11.169 ms
84 bytes from 10.1.1.1 icmp_seq=3 ttl=64 time=7.512 ms
^C

```

# II. VLAN

## 3. Setup topologie 2

Configuration du 3e vpcs:
```
PC3> ip 10.1.1.3/24
Checking for duplicate address...
PC3 : 10.1.1.3 255.255.255.0
```


On teste que les 3 vpcs sont connectées:
```
PC1> ping 10.1.1.2

84 bytes from 10.1.1.2 icmp_seq=1 ttl=64 time=8.982 ms
84 bytes from 10.1.1.2 icmp_seq=2 ttl=64 time=4.306 ms
^C
PC1> ping 10.1.1.3

84 bytes from 10.1.1.3 icmp_seq=1 ttl=64 time=12.474 ms
84 bytes from 10.1.1.3 icmp_seq=2 ttl=64 time=4.457 ms
^C
```
```
PC2> ping 10.1.1.3

84 bytes from 10.1.1.3 icmp_seq=1 ttl=64 time=9.073 ms
84 bytes from 10.1.1.3 icmp_seq=2 ttl=64 time=7.254 ms
^C
```

Configuration du switch:
```
Switch>enable
Switch#conf t
Enter configuration commands, one per line.  End with CNTL/Z.
Switch(config)#vlan 10
Switch(config-vlan)#name admins
Switch(config-vlan)#exit
Switch(config)#vlan 20
Switch(config-vlan)#name guests
Switch(config-vlan)#exit
Switch(config)#
```

Config des vlan : 
```
Switch(config)#interface Gi0/0
Switch(config-if)#switchport mode access
Switch(config-if)#switchport acess vlan 10
                               ^
% Invalid input detected at '^' marker.

Switch(config-if)#switchport access vlan 10
Switch(config-if)#exit
Switch(config)#interface Gi0/1
Switch(config-if)#switchport mode access
Switch(config-if)#switchport access vlan 10
Switch(config-if)#exit
Switch(config)#interface Gi0/2
Switch(config-if)#switchport mode access
Switch(config-if)#switchport access vlan 20
Switch(config-if)#exit
```
Vérif:
```
PC1> ping 10.1.1.2

84 bytes from 10.1.1.2 icmp_seq=1 ttl=64 time=7.091 ms
84 bytes from 10.1.1.2 icmp_seq=2 ttl=64 time=6.811 ms
^C
```

```
PC3> ping 10.1.1.1

host (10.1.1.1) not reachable
```

# III. Routing
Config vlan: 
```

Switch(config)#vlan 11
Switch(config-vlan)#name clients
Switch(config-vlan)#exit
Switch(config)#vlan 12
Switch(config-vlan)#name admins
VLAN #12 and #10 have an identical name: admins
Switch(config-vlan)#exit
Switch(config)#no vlan 10
Switch(config)#no vlan 20
Switch(config)#vlan 12
Switch(config-vlan)#name admins
Switch(config-vlan)#exit
Switch(config)#vlan 13
Switch(config-vlan)#name servers
Switch(config-vlan)#exit
Switch(config)#interface Gi0/0
Switch(config-if)#switchport mode access
Switch(config-if)#switchport access vlan 11
Switch(config-if)#exit
Switch(config)#interface Gi0/1
Switch(config-if)#switchport mode access
Switch(config-if)#switchport acess vlan 11
                               ^
% Invalid input detected at '^' marker.

Switch(config-if)#switchport access vlan 11
Switch(config-if)#exit
Switch(config)#interface Gi0/2
Switch(config-if)#switchport mode access
Switch(config-if)#switchport access vlan 12
Switch(config-if)#exit
Switch(config)#interface Gi0/3
Switch(config-if)#switchport mode access
Switch(config-if)#switchport access vlan 13
Switch(config-if)#exit
```
Port de trunk
```
Switch(config)#interface Gi3/3
Switch(config-if)#switchport trunk encapsulation dot1q
Switch(config-if)#switchport mode trunk
Switch(config-if)#switchport trunk allowed vlan add 11,12,13
Switch(config-if)#exit
Switch(config)#exit
Switch#sho
*Oct 21 13:07:47.389: %SYS-5-CONFIG_I: Configured from console by cons
Switch#show interface trunk

Port        Mode             Encapsulation  Status        Native vlan
Gi3/3       on               802.1q         trunking      1

Port        Vlans allowed on trunk
Gi3/3       1-4094

Port        Vlans allowed and active in management domain
Gi3/3       1,11-13

Port        Vlans in spanning tree forwarding state and not pruned
Gi3/3       1,11-13
```


config du routeur:

```
R1(config)#interface fastEthernet 0/0.11
R1(config-subif)#encapsulation dot1Q 11
R1(config-subif)#ip addr 10.1.1.254 255.255.255.0
R1(config-subif)#exit
R1(config)#interface fastEthernet 0/0.12
R1(config-subif)#encapsulation dot1Q 12
R1(config-subif)#ip addr 10.2.2.254 255.255.255.0
R1(config-subif)#exit
R1(config)#interface fastEthernet 0/0.13
R1(config-subif)#encapsulation dot1Q 13
R1(config-subif)#ip addr 10.3.3.254 255.255.255.0
R1(config-subif)#exit
R1(config)#exit
```
Ping du routeur :
```
PC1> ping 10.1.1.254

84 bytes from 10.1.1.254 icmp_seq=1 ttl=255 time=27.308 ms
84 bytes from 10.1.1.254 icmp_seq=2 ttl=255 time=14.464 ms


```

Configuration des routes par défaut:
sur les vpcs : commande `ip 10.x.x.x/24 10.x.x.254 255.255.255.0

sur web1 : `sudo ip route add default via 10.3.3.254 dev enp0s3`

Ping entre les réseaux:
```
[kraizix@web1 ~]$ ping 10.1.1.2
PING 10.1.1.2 (10.1.1.2) 56(84) bytes of data.
64 bytes from 10.1.1.2: icmp_seq=2 ttl=63 time=25.4 ms
64 bytes from 10.1.1.2: icmp_seq=3 ttl=63 time=28.10 ms
^C
--- 10.1.1.2 ping statistics ---
3 packets transmitted, 2 received, 33.3333% packet loss, time 2064ms
rtt min/avg/max/mdev = 25.411/27.202/28.993/1.791 ms
[kraizix@web1 ~]$ ping 10.2.2.1
PING 10.2.2.1 (10.2.2.1) 56(84) bytes of data.
64 bytes from 10.2.2.1: icmp_seq=1 ttl=63 time=24.5 ms
64 bytes from 10.2.2.1: icmp_seq=2 ttl=63 time=23.3 ms
64 bytes from 10.2.2.1: icmp_seq=3 ttl=63 time=32.1 ms
^C
--- 10.2.2.1 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2004ms
rtt min/avg/max/mdev = 23.256/26.625/32.139/3.930 ms
```
```
PC1> ping 10.1.1.2

84 bytes from 10.1.1.2 icmp_seq=1 ttl=64 time=21.229 ms
84 bytes from 10.1.1.2 icmp_seq=2 ttl=64 time=8.599 ms
84 bytes from 10.1.1.2 icmp_seq=3 ttl=64 time=4.468 ms
^C
PC1> ping 10.2.2.1

84 bytes from 10.2.2.1 icmp_seq=1 ttl=63 time=37.316 ms
84 bytes from 10.2.2.1 icmp_seq=2 ttl=63 time=36.161 ms
84 bytes from 10.2.2.1 icmp_seq=3 ttl=63 time=23.057 ms
^C
PC1> ping 10.3.3.1

84 bytes from 10.3.3.1 icmp_seq=1 ttl=63 time=35.905 ms
84 bytes from 10.3.3.1 icmp_seq=2 ttl=63 time=36.360 ms
84 bytes from 10.3.3.1 icmp_seq=3 ttl=63 time=30.129 ms
```

# IV. NAT

## 3. Setup topologie 4

Vérification de l'ip
```
R1#show ip int br
Interface                  IP-Address      OK? Method Status                Protocol
FastEthernet0/0            unassigned      YES NVRAM  up                    up
FastEthernet0/0.11         10.1.1.254      YES NVRAM  up                    up
FastEthernet0/0.12         10.2.2.254      YES NVRAM  up                    up
FastEthernet0/0.13         10.3.3.254      YES NVRAM  up                    up
FastEthernet1/0            10.0.3.16       YES DHCP   up                    up
FastEthernet2/0            unassigned      YES NVRAM  administratively down down
FastEthernet3/0            unassigned      YES NVRAM  administratively down down


```
Ping Cloudflare
```
R1#ping 1.1.1.1

Type escape sequence to abort.
Sending 5, 100-byte ICMP Echos to 1.1.1.1, timeout is 2 seconds:
.!!!!
Success rate is 80 percent (4/5), round-trip min/avg/max = 24/34/40 ms
```

Configuration de la nat :

```
R1(config)#interface fastEthernet 1/0
R1(config-if)#ip nat outside

*Mar  1 00:07:49.219: %LINEPROTO-5-UPDOWN: Line protocol on Interface NVI0, changed state to up
R1(config-if)#
R1(config-if)#
*Mar  1 00:07:56.007: %SYS-3-CPUHOG: Task is running for (2036)msecs, more than (2000)msecs (1/1),process = Exec.
-Traceback= 0x612C9CD0 0x612CA974 0x61292050 0x61292310 0x61292434 0x61292434 0x61293304 0x612C6154 0x612D234C 0x612BC744 0x612BD3A8 0x612BE2F8 0x60F0A454 0x6040914C 0x60425410 0x604C8600
*Mar  1 00:07:56.447: %SYS-3-CPUYLD: Task ran for (2476)msecs, more than (2000)msecs (1/1),process = Exec
R1(config-if)#exit
R1(config)#access-list 1 permit any
R1(config)#ip nat inside source list 1 interface fastEthernet 1/0 overload

```

Ajout d'un DNS :

On utilise la commande `ip dns 8.8.8.8` sur les vpcs

On modifie le fichier /etc/resolv.conf sur la vm

ping depuis la vm :
```
[kraizix@web1 ~]$ ping google.com
PING google.com (216.58.209.238) 56(84) bytes of data.
64 bytes from par10s29-in-f14.1e100.net (216.58.209.238): icmp_seq=1 ttl=112 time=43.2 ms
64 bytes from par10s29-in-f14.1e100.net (216.58.209.238): icmp_seq=2 ttl=112 time=46.1 ms
64 bytes from par10s29-in-f14.1e100.net (216.58.209.238): icmp_seq=3 ttl=112 time=73.5 ms
64 bytes from par10s29-in-f14.1e100.net (216.58.209.238): icmp_seq=4 ttl=112 time=38.2 ms

```
ping depuis un vpcs :
```
PC1> ping google.com
google.com resolved to 216.58.198.206

84 bytes from 216.58.198.206 icmp_seq=1 ttl=113 time=50.180 ms
84 bytes from 216.58.198.206 icmp_seq=2 ttl=113 time=43.243 ms
84 bytes from 216.58.198.206 icmp_seq=3 ttl=113 time=48.800 ms
^C
```

# V. Add a building


Création du serveur DHCP :
- Ajout des packets : `sudo dnf -y install dhcp-server`
- Création du fichier de conf : `sudo vim /etc/dhcp/dhcpd.conf` 

```
# DHCP Server Configuration file.
#   see /usr/share/doc/dhcp-server/dhcpd.conf.example
#   see dhcpd.conf(5) man page
#
option domain-name     "clients.tp4";
# specify DNS server's hostname or IP address
option domain-name-servers     dhcp1.clients.tp4;
# default lease time
default-lease-time 600;
# max lease time
max-lease-time 7200;
# this DHCP server to be declared valid
authoritative;
# specify network address and subnetmask
subnet 10.1.1.0 netmask 255.255.255.0 {
    # specify the range of lease IP address
    range dynamic-bootp 10.1.1.20 10.1.1.253;
    # specify gateway
    option routers 10.1.1.254;
    option domain-name-servers 1.1.1.1;}
```
- Démarrage du service : `systemctl enable --now dhcpd`
- Ajout du port au firewall : `firewall-cmd --add-port=67/udp` & `firewall-cmd --add-port=67/udp --permanent`


Vérification du serveur dhcp :
```
PC5> ip dhcp
DDORA IP 10.1.1.20/24 GW 10.1.1.254

PC5> ping 8.8.8.8

8.8.8.8 icmp_seq=1 timeout
84 bytes from 8.8.8.8 icmp_seq=2 ttl=112 time=55.142 ms
84 bytes from 8.8.8.8 icmp_seq=3 ttl=112 time=57.863 ms
84 bytes from 8.8.8.8 icmp_seq=4 ttl=112 time=61.542 ms
^C
PC5> ping google.com
google.com resolved to 216.58.204.110

84 bytes from 216.58.204.110 icmp_seq=1 ttl=112 time=58.392 ms
84 bytes from 216.58.204.110 icmp_seq=2 ttl=112 time=71.213 ms
84 bytes from 216.58.204.110 icmp_seq=3 ttl=112 time=57.884 ms
^C
PC5> ping 10.3.3.1

84 bytes from 10.3.3.1 icmp_seq=1 ttl=63 time=49.558 ms
84 bytes from 10.3.3.1 icmp_seq=2 ttl=63 time=61.621 ms
84 bytes from 10.3.3.1 icmp_seq=3 ttl=63 time=67.220 ms
```