# TP2-Réseau

- [TP2-Réseau](#tp2-réseau)
  - [I. ARP](#i-arp)
    - [1. Echange ARP :](#1-echange-arp-)
    - [2. Analyse de trames :](#2-analyse-de-trames-)
  - [II. Routage](#ii-routage)
    - [1.Mise en place du routage :](#1mise-en-place-du-routage-)
    - [2.Analyse de trames :](#2analyse-de-trames-)
    - [3.Accès Internet :](#3accès-internet-)
  - [III. DHCP](#iii-dhcp)
    - [1. Mise en place du serveur DHCP :](#1-mise-en-place-du-serveur-dhcp-)
    - [2. Analyse de trames :](#2-analyse-de-trames--1)


##  I. ARP
### 1. Echange ARP :
Pings de  node1 vers node2
```
[kraizix@node1 ~]$ ping 10.2.1.12
PING 10.2.1.12 (10.2.1.12) 56(84) bytes of data.
64 bytes from 10.2.1.12: icmp_seq=1 ttl=64 time=0.872 ms
64 bytes from 10.2.1.12: icmp_seq=2 ttl=64 time=0.633 ms
64 bytes from 10.2.1.12: icmp_seq=3 ttl=64 time=0.690 ms
64 bytes from 10.2.1.12: icmp_seq=4 ttl=64 time=0.658 ms
64 bytes from 10.2.1.12: icmp_seq=5 ttl=64 time=0.621 ms
^C
--- 10.2.1.12 ping statistics ---
5 packets transmitted, 5 received, 0% packet loss, time 4094ms
rtt min/avg/max/mdev = 0.621/0.694/0.872/0.097 ms
```

Table ARP de node 1 :
```
[kraizix@node1 ~]$ ip n s
10.2.1.12 dev enp0s8 lladdr 08:00:27:79:96:c8 REACHABLE
10.2.1.1 dev enp0s8 lladdr 0a:00:27:00:00:10 REACHABLE
```
On obtient l'adresse Mac : 08:00:27:79:96:c8
```
[kraizix@node2 ~]$ ip a
[...]
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:79:96:c8 brd ff:ff:ff:ff:ff:ff
    inet 10.2.1.12/24 brd 10.2.1.255 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe79:96c8/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
```
L'adresse Mac de node2 correspond à celle trouvé dans la table ARP de node 1

Table ARP de node 2 :
```
[kraizix@node2 ~]$ ip n s
10.2.1.11 dev enp0s8 lladdr 08:00:27:2f:66:77 STALE
10.2.1.1 dev enp0s8 lladdr 0a:00:27:00:00:10 DELAY
```
On obtient l'adresse Mac : 08:00:27:2f:66:77

```
[kraizix@node1 ~]$ ip a
[...]
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:2f:66:77 brd ff:ff:ff:ff:ff:ff
    inet 10.2.1.11/24 brd 10.2.1.255 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe2f:6677/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
```

L'adresse Mac de node1 correspond à celle trouvé dans la table ARP de node 2

### 2. Analyse de trames :
Commande : `sudo tcpdump -i enp0s8 -w tp2_arp.pcap`

| ordre | type trame  | source                      | destination                 |
| ----- | ----------- | --------------------------- | --------------------------- |
| 1     | Requête ARP | `node1` `08:00:27:2f:66:77` | Broadcast `FF:FF:FF:FF:FF`  |
| 2     | Réponse ARP | `node2` `08:00:27:79:96:c8` | `node1` `08:00:27:2f:66:77` |

📁 Capture réseau [tp2_arp.pcap](captures/tp2_arp.pcap)
## II. Routage

### 1.Mise en place du routage :
* Activation du routage sur le noeud router.net2.tp2
```
[kraizix@router ~]$ sudo firewall-cmd --add-masquerade --zone=public --permanent
success
[kraizix@router ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8 enp0s9
  sources:
  services: cockpit dhcpv6-client ssh
  ports:
  protocols:
  masquerade: yes
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```

* Ajout des routes statiques :

sur node1 :
```
[kraizix@node1 ~]$ ping 10.2.2.12
PING 10.2.2.12 (10.2.2.12) 56(84) bytes of data.
64 bytes from 10.2.2.12: icmp_seq=1 ttl=63 time=2.85 ms
64 bytes from 10.2.2.12: icmp_seq=2 ttl=63 time=1.20 ms
64 bytes from 10.2.2.12: icmp_seq=3 ttl=63 time=1.42 ms
^C
--- 10.2.2.12 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2003ms
rtt min/avg/max/mdev = 1.201/1.821/2.845/0.730 ms
[kraizix@node1 ~]$ cat /etc/sysconfig/network-scripts/route-enp0s8
10.2.2.12/24 via 10.2.1.254 dev enp0s8
[kraizix@node1 ~]$ ip r s
10.2.1.0/24 dev enp0s8 proto kernel scope link src 10.2.1.11 metric 100
10.2.2.0/24 via 10.2.1.254 dev enp0s8 proto static metric 100
```

```
[kraizix@marcel ~]$ ping 10.2.1.11
PING 10.2.1.11 (10.2.1.11) 56(84) bytes of data.
64 bytes from 10.2.1.11: icmp_seq=1 ttl=63 time=1.14 ms
64 bytes from 10.2.1.11: icmp_seq=2 ttl=63 time=1.14 ms
64 bytes from 10.2.1.11: icmp_seq=3 ttl=63 time=1.28 ms
^C
--- 10.2.1.11 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2004ms
rtt min/avg/max/mdev = 1.135/1.183/1.279/0.078 ms
[kraizix@marcel ~]$ cat /etc/sysconfig/network-scripts/route-enp0s8
10.2.1.11/24 via 10.2.2.254 dev enp0s8
[kraizix@marcel ~]$ ip r s
10.2.1.0/24 via 10.2.2.254 dev enp0s8 proto static metric 100
10.2.2.0/24 dev enp0s8 proto kernel scope link src 10.2.2.12 metric 100
```
### 2.Analyse de trames :
node 1:
```
[kraizix@node1 ~]$ ping 10.2.2.12
PING 10.2.2.12 (10.2.2.12) 56(84) bytes of data.
64 bytes from 10.2.2.12: icmp_seq=1 ttl=63 time=1.79 ms
64 bytes from 10.2.2.12: icmp_seq=2 ttl=63 time=1.07 ms
64 bytes from 10.2.2.12: icmp_seq=3 ttl=63 time=1.06 ms
^C
--- 10.2.2.12 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2003ms
rtt min/avg/max/mdev = 1.059/1.305/1.786/0.340 ms
[kraizix@node1 ~]$ ip n s
10.2.1.1 dev enp0s8 lladdr 0a:00:27:00:00:10 REACHABLE
10.2.1.254 dev enp0s8 lladdr 08:00:27:59:ee:36 REACHABLE
```
marcel:
```
[kraizix@marcel ~]$ ip n s
10.2.2.254 dev enp0s8 lladdr 08:00:27:03:0c:82 REACHABLE
10.2.2.1 dev enp0s8 lladdr 0a:00:27:00:00:47 DELAY
```
router:
```
[kraizix@router ~]$ ip n s
10.2.2.12 dev enp0s9 lladdr 08:00:27:79:96:c8 REACHABLE
10.2.1.1 dev enp0s8 lladdr 0a:00:27:00:00:10 DELAY
10.2.1.11 dev enp0s8 lladdr 08:00:27:2f:66:77 REACHABLE
```

| ordre | type trame  | IP source  | MAC source                   | IP destination | MAC destination              |
| ----- | ----------- | ---------- | ---------------------------- | -------------- | ---------------------------- |
| 1     | Requête ARP | 10.2.1.11  | `node1` `08:00:27:2f:66:77`  | 10.2.1.254     | Broadcast `FF:FF:FF:FF:FF`   |
| 2     | Réponse ARP | 10.2.1.254 | `router` `08:00:27:59:ee:36` | 10.2.1.11      | `node1` `08:00:27:2f:66:77`  |
| 3     | Ping        | 10.2.1.11  | `node1` `08:00:27:2f:66:77`  | 10.2.1.254     | `router` `08:00:27:59:ee:36` |
| 4     | Requête ARP | 10.2.2.254 | `router` `08:00:27:59:ee:36` | 10.2.2.12      | `marcel` `08:00:27:03:0c:82` |
| 5     | Réponse ARP | 10.2.2.12  | `marcel` `08:00:27:03:0c:82` | 10.2.2.254     | `router` `08:00:27:59:ee:36` |
| 6     | Ping        | 10.2.2.254 | `router` `08:00:27:59:ee:36` | 10.2.2.12      | `marcel` `08:00:27:03:0c:82` |
| 7     | Pong        | 10.2.2.12  | `marcel` `08:00:27:03:0c:82` | 10.2.2.254     | `router` `08:00:27:59:ee:36` |
| 8     | Pong        | 10.2.1.254 | `router` `08:00:27:59:ee:36` | 10.2.1.11      | `node1` `08:00:27:2f:66:77`  |

📁 Capture réseau [tp2_routage_node1.pcap](captures/tp2_routage_node1.pcap), [tp2_routage_marcel.pcap](captures/tp2_routage_marcel.pcap)


### 3.Accès Internet :

Ajout des routes statiques :
commande : sudo nano /etc/sysconfig/network-scripts/route-enp0s8
Il faut rajouter : `10.2.2.12/24 via 10.2.1.254 dev enp0s8` dans le fichier `route-enp0s8` pour node1
Il faut rajouter : `10.2.1.11/24 via 10.2.2.254 dev enp0s8d` dans le fichier `route-enp0s8` pour marcel

Ajout du serveur DNS :
commande : `sudo nano /etc/resolv.conf` et rajouter la ligne `nameserver 1.1.1.1`sur chacune des machines

Test réseau et DNS :
Marcel:
```
[kraizix@marcel ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=113 time=20.1 ms
^C
--- 8.8.8.8 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 20.112/20.112/20.112/0.000 ms
[kraizix@marcel ~]$ ping google.com
PING google.com (216.58.209.238) 56(84) bytes of data.
64 bytes from par10s29-in-f238.1e100.net (216.58.209.238): icmp_seq=1 ttl=112 time=20.2 ms
^C
--- google.com ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 20.192/20.192/20.192/0.000 ms

```

node1

```
[kraizix@node1 ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=113 time=19.3 ms
^C
--- 8.8.8.8 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 19.252/19.252/19.252/0.000 ms
[kraizix@node1 ~]$ ping google.com
PING google.com (142.250.75.238) 56(84) bytes of data.
64 bytes from par10s41-in-f14.1e100.net (142.250.75.238): icmp_seq=1 ttl=113 time=18.6 ms
64 bytes from par10s41-in-f14.1e100.net (142.250.75.238): icmp_seq=2 ttl=113 time=18.5 ms
^C
--- google.com ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1001ms
rtt min/avg/max/mdev = 18.450/18.549/18.648/0.099 ms

```

| ordre | type trame | IP source           | MAC source                  | IP destination      | MAC destination             |     |
| ----- | ---------- | ------------------- | --------------------------- | ------------------- | --------------------------- | --- |
| 1     | ping       | `node1` `10.2.1.12` | `node1` `08:00:27:2f:66:77` | `8.8.8.8`           | 08:00:27:59:ee:36           |     |
| 2     | pong       | `8.8.8.8`           | `08:00:27:59:ee:36`         | `node1` `10.2.1.12` | `node1` `08:00:27:2f:66:77` | ... |

📁 Capture réseau [tp2_routage_internet.pcap](captures/tp2_routage_internet.pcap)
## III. DHCP

### 1. Mise en place du serveur DHCP :
Instalation:
`dnf -y install dhcp-server`

`vi /etc/dhcp/dhcpd.conf`

On ajoute les lignes suivantes dans le fichier :

```
default-lease-time 900;
max-lease-time 10800;
ddns-update-style none;
authoritative;
subnet 10.2.1.0 netmask 255.255.255.0 {
        range dynamic-bootp 10.2.1.2 10.2.1.253;
        option routers 10.2.1.254;
}

```

Autorisation du service DHCP dans le firewall
`sudo firewall-cmd --add-service=dhcp`
`sudo firewall-cmd --runtime-to-permanent`

Vérification des ip attribuées par le serveur dhcp
```
[kraizix@node1 ~]$ cat /var/lib/dhcpd/dhcpd.leases
# The format of this file is documented in the dhcpd.leases(5) manual page.
# This lease file was written by isc-dhcp-4.3.6

# authoring-byte-order entry is generated, DO NOT DELETE
authoring-byte-order little-endian;

server-duid "\000\001\000\001(\337Ob\010\000'/fw";

lease 10.2.1.2 {
  starts 4 2021/09/23 14:48:52;
  ends 4 2021/09/23 15:03:52;
  cltt 4 2021/09/23 14:48:52;
  binding state active;
  next binding state free;
  rewind binding state free;
  hardware ethernet 08:00:27:09:be:f3;
  uid "\001\010\000'\011\276\363";
  client-hostname "node2";
}

```
* Améliorer la configuration du DHCP

Configuration :
```
#
# DHCP Server Configuration file.
#   see /usr/share/doc/dhcp-server/dhcpd.conf.example
#   see dhcpd.conf(5) man page
#
default-lease-time 900;
max-lease-time 10800;
ddns-update-style none;
authoritative;
option domain-name-servers 1.1.1.1;
subnet 10.2.1.0 netmask 255.255.255.0 {
        range dynamic-bootp 10.2.1.2 10.2.1.253;
        option routers 10.2.1.254;
}

```

Vérification IP :
commande : `ip a`
```
[kraizix@node2 ~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:09:be:f3 brd ff:ff:ff:ff:ff:ff
    inet 10.2.1.2/24 brd 10.2.1.255 scope global dynamic noprefixroute enp0s8
       valid_lft 739sec preferred_lft 739sec
    inet 10.2.1.3/24 brd 10.2.1.255 scope global secondary dynamic enp0s8
       valid_lft 586sec preferred_lft 586sec
    inet6 fe80::a00:27ff:fe09:bef3/64 scope link noprefixroute
       valid_lft forever preferred_lft forever

```
ping passerelle :

```
[kraizix@node2 ~]$ ping 10.2.1.254
PING 10.2.1.254 (10.2.1.254) 56(84) bytes of data.
64 bytes from 10.2.1.254: icmp_seq=1 ttl=64 time=1.08 ms
64 bytes from 10.2.1.254: icmp_seq=2 ttl=64 time=0.696 ms
^C
--- 10.2.1.254 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1002ms
rtt min/avg/max/mdev = 0.696/0.887/1.079/0.193 ms

```


* Route par défaut
```
[kraizix@node2 ~]$ ip r
default via 10.2.1.254 dev enp0s8
default via 10.2.1.254 dev enp0s8 proto dhcp metric 101
10.2.1.0/24 dev enp0s8 proto kernel scope link src 10.2.1.2 metric 101
```
ping d'une IP
```
[kraizix@node2 ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=113 time=19.6 ms
^C
--- 8.8.8.8 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 19.599/19.599/19.599/0.000 ms

```

* Serveur DNS

```

[kraizix@node2 ~]$ sudo dig google.com

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> google.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 46803
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
;; QUESTION SECTION:
;google.com.                    IN      A

;; ANSWER SECTION:
google.com.             217     IN      A       216.58.198.206

;; Query time: 18 msec
;; SERVER: 1.1.1.1#53(1.1.1.1)
;; WHEN: Thu Sep 23 17:58:40 CEST 2021
;; MSG SIZE  rcvd: 55

```

ping vers un nom de domaine :
```
[kraizix@node2 ~]$ ping google.com
PING google.com (216.58.214.78) 56(84) bytes of data.
64 bytes from fra15s10-in-f78.1e100.net (216.58.214.78): icmp_seq=1 ttl=113 time=20.7 ms
^C
--- google.com ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 20.651/20.651/20.651/0.000 ms
```

### 2. Analyse de trames :
Pour récupèrer une nouvelle adresse auprès du serveur DHCP il faut d'abord libèrer l'adresse IP avec la commande : `sudo dhclient -r`
puis utiliser la commande `sudo dhclient` pour en récupèrer une.

| ordre | type trame    | IP source | MAC source                  | IP destination  | MAC destination                 |
| ----- | ------------- | --------- | --------------------------- | --------------- | ------------------------------- |
| 1     | DHCP Discover | 0.0.0.0   | `08:00:27:09:be:f3`         | 255.555.255.255 | `broadcast` `ff:ff:ff:ff:ff:ff` |
| 2     | ARP           | 10.2.1.11 | `node1` `08:00:27:2f:66:77` | 10.2.1.3        | Broadcast `FF:FF:FF:FF:FF`      |
| 3     | DHCP Offer    | 10.2.1.11 | `node1` `08:00:27:2f:66:77` | 10.2.1.3        | `node2` `08:00:27:09:be:f3`     |
| 4     | DHCP Request  | 0.0.0.0   | `08:00:27:09:be:f3`         | 255.555.255.255 | `broadcast` `ff:ff:ff:ff:ff:ff` |
| 5     | DHCP ACK      | 10.2.1.11 | `node1` `08:00:27:2f:66:77` | 10.2.1.3        | `node2` `08:00:27:09:be:f3`     |

📁 Capture réseau [tp2_dhcp.pcap](captures/tp2_dhcp.pcap)