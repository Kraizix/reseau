# TP3-Réseau

- [TP3-Réseau](#tp3-réseau)
- [I. (mini)Architecture réseau](#i-miniarchitecture-réseau)
- [II. Services d'infra](#ii-services-dinfra)
  - [1. Serveur DHCP](#1-serveur-dhcp)
  - [2. Serveur DNS](#2-serveur-dns)
    - [A. Our own DNS server](#a-our-own-dns-server)
    - [B.SETUP copain](#bsetup-copain)
    - [3. Get deeper](#3-get-deeper)
- [III. Services métier](#iii-services-métier)
  - [1. Serveur Web](#1-serveur-web)
  - [2. Partage de fichiers](#2-partage-de-fichiers)
    - [B. Le setup wola](#b-le-setup-wola)
- [IV. Un peu de théorie : TCP et UDP](#iv-un-peu-de-théorie--tcp-et-udp)
- [V. El final](#v-el-final)

# I. (mini)Architecture réseau

Routeur :

vérification de l'IP dans chaque réseaux : 

commande : `ip a`
```
[kraizix@router ~]$ ip a
[...]
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:10:97:ea brd ff:ff:ff:ff:ff:ff
    inet 10.3.1.190/26 brd 10.3.1.191 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe10:97ea/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
4: enp0s9: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:e6:fd:b6 brd ff:ff:ff:ff:ff:ff
    inet 10.3.1.126/25 brd 10.3.1.127 scope global noprefixroute enp0s9
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fee6:fdb6/64 scope link
       valid_lft forever preferred_lft forever
5: enp0s10: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:86:d2:b9 brd ff:ff:ff:ff:ff:ff
    inet 10.3.1.206/28 brd 10.3.1.207 scope global noprefixroute enp0s10
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe86:d2b9/64 scope link
       valid_lft forever preferred_lft forever
```

Vérification de l'accès internet : 

```
[kraizix@router ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=114 time=21.6 ms
^C
--- 8.8.8.8 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 21.560/21.560/21.560/0.000 ms

```

Vérification de la résolution de nom :
```
[kraizix@router ~]$ dig google.com

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> google.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 43007
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 4000
;; QUESTION SECTION:
;google.com.                    IN      A

;; ANSWER SECTION:
google.com.             112     IN      A       142.250.178.142

;; Query time: 2 msec
;; SERVER: 10.33.10.2#53(10.33.10.2)
;; WHEN: Mon Sep 27 16:34:03 CEST 2021
;; MSG SIZE  rcvd: 55

```
Vérification du nom de la machine :
```
[kraizix@router ~]$ hostname
router.tp3
```
Activation du routage: 
```
sudo firewall-cmd --add-masquerade --zone=public
sudo firewall-cmd --add-masquerade --zone=public --permanent
```

```
[kraizix@router ~]$ sudo firewall-cmd --list-all
[sudo] password for kraizix:
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s10 enp0s3 enp0s8 enp0s9
  sources:
  services: cockpit dhcpv6-client ssh
  ports:
  protocols:
  masquerade: yes
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```


# II. Services d'infra

## 1. Serveur DHCP
📁 Fichier [dhcpd.conf](ressources/dhcpd.conf)

Accès internet du client dans le réseau + DNS :

```
[kraizix@marcel ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=113 time=19.7 ms
^C
--- 8.8.8.8 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 19.741/19.741/19.741/0.000 ms
[kraizix@marcel ~]$ ping google.com
PING google.com (216.58.209.238) 56(84) bytes of data.
64 bytes from par10s29-in-f14.1e100.net (216.58.209.238): icmp_seq=1 ttl=112 time=19.2 ms
^C
--- google.com ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 19.207/19.207/19.207/0.000 ms
```
Traceroute pour vérifier la passerelle
```
[kraizix@marcel ~]$ traceroute google.com
traceroute to google.com (142.250.75.238), 30 hops max, 60 byte packets
 1  _gateway (10.3.1.190)  4.203 ms  3.993 ms  3.502 ms
 2  10.0.2.2 (10.0.2.2)  3.288 ms  3.085 ms  2.845 ms
 3  10.0.2.2 (10.0.2.2)  4.628 ms * *
```

## 2. Serveur DNS

### A. Our own DNS server

`sudo dnf install -y bind bind-utils`


### B.SETUP copain
**Mise en place : **

Installation de packets : `sudo dnf -y install bind bind-utils`

Modification du fichier de conf: 
`sudo vim /etc/named.conf`

```
[kraizix@dns1 ~]$ sudo cat /etc/named.conf
[sudo] password for kraizix:
//
// named.conf
//
// Provided by Red Hat bind package to configure the ISC BIND named(8) DNS
// server as a caching only nameserver (as a localhost DNS resolver only).
//
// See /usr/share/doc/bind*/sample/ for example named configuration files.
//

options {

        listen-on port 53 { any; };
        listen-on-v6 port 53 { ::1; };
        directory       "/var/named";
        dump-file       "/var/named/data/cache_dump.db";
        statistics-file "/var/named/data/named_stats.txt";
        memstatistics-file "/var/named/data/named_mem_stats.txt";
        secroots-file   "/var/named/data/named.secroots";
        recursing-file  "/var/named/data/named.recursing";
        allow-query     { localhost;10.3.1.0/25;10.3.1.192/28; };
        /*
         - If you are building an AUTHORITATIVE DNS server, do NOT enable recursion.
         - If you are building a RECURSIVE (caching) DNS server, you need to enable
           recursion.
         - If your recursive DNS server has a public IP address, you MUST enable access
           control to limit queries to your legitimate users. Failing to do so will
           cause your server to become part of large scale DNS amplification

           attacks. Implementing BCP38 within your network would greatly
           reduce such attack surface
        */
        recursion yes;

        dnssec-enable yes;
        dnssec-validation yes;

        managed-keys-directory "/var/named/dynamic";

        pid-file "/run/named/named.pid";
        session-keyfile "/run/named/session.key";

        /* https://fedoraproject.org/wiki/Changes/CryptoPolicy */
        include "/etc/crypto-policies/back-ends/bind.config";
};

logging {
        channel default_debug {
                file "data/named.run";
                severity dynamic;
        };
};

zone "." IN {
        type hint;
        file "named.ca";
};

include "/etc/named.rfc1912.zones";
include "/etc/named.root.key";

zone "server1.tp3" IN {
        type master;
        file "/var/named/server1.tp3.forward";
        allow-update { none; };
};

zone "server2.tp3" IN {
        type master;
        file "/var/named/server2.tp3.forward";
        allow-update { none; };
};
```
Création des fichiers de forward `server1.tp3.forward`,`server2.tp3.forward`:
```
[kraizix@dns1 ~]$ sudo cat /var/named/server1.tp3.forward
[sudo] password for kraizix:

$TTL 86400
@   IN  SOA     dns1.server1.tp3. root.dns1.server1.tp3. (


        2021072301  ;Serial
        3600        ;Refresh
        1800        ;Retry
        604800      ;Expire
        86400)       ;Minimum TTL
        IN  NS      dns1.server1.tp3.
        IN  A       10.3.1.2



router  IN      A       10.3.1.126
dns1    IN      A       10.3.1.2
```
```
[kraizix@dns1 ~]$ sudo cat /var/named/server2.tp3.forward
$TTL 86400
@   IN  SOA     dns1.server2.tp3. root.dns1.server2.tp3. (


        2021072301  ;Serial
        3600        ;Refresh
        1800        ;Retry
        604800      ;Expire
        86400)       ;Minimum TTL
        IN  NS      dns1.
        IN  A       10.3.1.2



router  IN      A       10.3.1.206
web1    IN      A       10.3.1.194
nfs1    IN      A       10.3.1.195
```
Démarrage du service named:
`sudo systemctl enable --now named`

Ajout des ports au firewall:
```
[kraizix@dns1 ~]$ sudo firewall-cmd --add-port=53/tcp
success
[kraizix@dns1 ~]$ sudo firewall-cmd --add-port=53/udp
success
[kraizix@dns1 ~]$ sudo firewall-cmd --add-port=53/tcp --permanent
success
[kraizix@dns1 ~]$ sudo firewall-cmd --add-port=53/udp --permanent
success

```

**Test depuis marcel :**

```
[kraizix@marcel ~]$ dig router.server1.tp3

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> router.server1.tp3
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 10702
;; flags: qr aa rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 1, ADDITIONAL: 2

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
; COOKIE: af7efa3122d1f37d1f74514c6155d8a949937d5c55935900 (good)
;; QUESTION SECTION:
;router.server1.tp3.            IN      A

;; ANSWER SECTION:
router.server1.tp3.     86400   IN      A       10.3.1.126

;; AUTHORITY SECTION:
server1.tp3.            86400   IN      NS      dns1.server1.tp3.

;; ADDITIONAL SECTION:
dns1.server1.tp3.       86400   IN      A       10.3.1.2

;; Query time: 1 msec
;; SERVER: 10.3.1.2#53(10.3.1.2)
;; WHEN: Thu Sep 30 17:32:55 CEST 2021
;; MSG SIZE  rcvd: 126
```

### 3. Get deeper
On ajoute ces 2 lignes au fichier de conf de named + mettre recursion sur yes : `sudo vim /etc/named.conf` 
```
   recursion yes;
   forwarders {8.8.8.8;};
	forward only;
```

```
[kraizix@marcel ~]$ dig google.com

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> google.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: SERVFAIL, id: 39635
;; flags: qr rd ra; QUERY: 1, ANSWER: 0, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
; COOKIE: 2d353277b700f4949ecfdaa86155db9b9fbd4614af9ec689 (good)
;; QUESTION SECTION:
;google.com.                    IN      A

;; Query time: 1 msec
;; SERVER: 10.3.1.2#53(10.3.1.2)
;; WHEN: Thu Sep 30 17:45:29 CEST 2021
;; MSG SIZE  rcvd: 67

```

# III. Services métier

## 1. Serveur Web

On installe un serveur nginx sur la machine
On ajoute le port 80 au firewall:
```
[kraizix@web1 ~]$ sudo firewall-cmd --add-port=80/tcp
[kraizix@web1 ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
```

On test le site en effectuant une requête avec la commande `curl` vers notre serveur web

```
[kraizix@marcel ~]$ curl web1.server2.tp3:80
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
  <head>
    <title>Test Page for the Nginx HTTP Server on Rocky Linux</title>
```

## 2. Partage de fichiers

### B. Le setup wola

**Côté serveur :**

Installation des packets `nfs-utils` :

```
[kraizix@nfs1 ~]$ sudo dnf -y install nfs-utils
```
`sudo vim /etc/idmapd.conf`


Création du dossier de partage :
`[kraizix@nfs1 ~]$ sudo mkdir -p /srv/nfs_share`

Ajout du dossier sur le partage en créant le fichier /etc/exports :

```
[kraizix@nfs1 ~]$ sudo vim /etc/exports
[kraizix@nfs1 ~]$ cat /etc/exports
/srv/nfs_share 10.3.1.192/28(rw,no_root_squash)
```

Activation du service au boot de la machine :

```
[kraizix@nfs1 ~]$ sudo systemctl enable --now rpcbind nfs-server
Created symlink /etc/systemd/system/multi-user.target.wants/nfs-server.service → /usr/lib/systemd/system/nfs-server.service.
```
Ouverture des ports sur le firewall :

```
[kraizix@nfs1 ~]$ sudo firewall-cmd --add-port=2049/tcp --permanent
success
[kraizix@nfs1 ~]$ sudo firewall-cmd --add-port=2049/tcp
success
```

**Côté client**

Installation des packets
```
[kraizix@web1 ~]$ sudo dnf -y install nfs-utils
```

Création du fichier de partage: 
```
[kraizix@web1 ~]$ sudo mkdir -p /srv/nfs
```
Montage du dossier :
```
[kraizix@web1 ~]$ sudo mount -t nfs 10.3.1.195:/srv/nfs_share /srv/nfs
```
Montage automique du dossier au boot de la machine:
`sudo vi /etc/fstab`
```
[kraizix@web1 ~]$ cat /etc/fstab

#
# /etc/fstab
# Created by anaconda on Wed Sep 15 13:25:17 2021
#
# Accessible filesystems, by reference, are maintained under '/dev/disk/'.
# See man pages fstab(5), findfs(8), mount(8) and/or blkid(8) for more info.
#
# After editing this file, run 'systemctl daemon-reload' to update systemd
# units generated from this file.
#
/dev/mapper/rl-root     /                       xfs     defaults        0 0
UUID=44569c3d-979b-4db7-94fd-740d13789a72 /boot                   xfs     defaults        0 0
/dev/mapper/rl-swap     none                    swap    defaults        0 0
10.3.1.195:/srv/nfs_share /srv/nfs_share
```

Test du partage de fichier : 
On créer un fichier depuis la machine web :
```
[kraizix@web1 ~]$ sudo touch /srv/nfs/file1
[kraizix@web1 ~]$ ls /srv/nfs
file1
```
On vérifie ensuite que le fichier est présent dans le dossier de partage du serveur :
```
[kraizix@nfs1 etc]$ ls /srv/nfs_share
file1
```
# IV. Un peu de théorie : TCP et UDP

Les protocoles ssh, http et nfs sont encapsulés dans du TCP

Le protocole dns est encapsulé dans du UDP

📁 Captures réseau [tp3_ssh.pcap](ressources/tp3_ssh.pcap), [tp3_http.pcap](ressources/tp3_http.pcap), [tp3_dns.pcap](ressources/tp3_dns.pcap) et [tp3_nfs.pcap](ressources/tp3_nfs.pcap)

📁 Capture réseau [tp3_3way.pcap](ressources/tp3_3way.pcap)
# V. El final

![](ressources/schema.png)

- 📁 Fichiers de zone:
  - [server1.tp3.forward](ressources/server1.tp3.forward)
  - [server2.tp3.forward](ressources/server2.tp3.forward)
- 📁 Fichier de conf principal DNS [named.conf](ressources/named.conf)


> Tableau des réseaux:

| Nom du réseau | Adresse du réseau | Masque            | Nombre de clients possibles | Adresse passerelle | Adresse broadcast |
| ------------- | ----------------- | ----------------- | --------------------------- | ------------------ | ----------------- |
| `client1`     | `10.3.1.128`      | `255.255.255.192` | 62                          | `10.3.1.190`       | `10.3.1.191`      |
| `server1`     | `10.3.1.0`        | `255.255.255.128` | 126                         | `10.3.1.126`       | `10.3.1.127`      |
| `server2`     | `10.3.1.192`      | `255.255.255.240` | 14                          | `10.3.1.206`       | `10.3.1.207`      |


 Tableau d'adressage
| Nom machine  | Adresse IP `client1` | Adresse IP `server1` | Adresse IP `server2` | Adresse de passerelle |
| ------------ | -------------------- | -------------------- | -------------------- | --------------------- |
| `router.tp3` | `10.3.1.190/26`      | `10.3.1.126/25`      | `10.3.1.206/28`      | Carte NAT             |
| dhcp.tp3     | 10.3.1.130/26        | x                    | x                    | 10.3.1.190/26         |
| marcel.tp3   | 10.3.1.131/26        | x                    | x                    | 10.3.1.190/26         |
| johnny.tp3   | 10.3.1.132/26        | x                    | x                    | 10.3.1.190/26         |
| dns1.tp3     | x                    | 10.3.1.2/25          | x                    | 10.3.1.126/25         |
| web1.tp3     | x                    | x                    | 10.3.1.194/28        | 10.3.1.206/28         |
| nfs1.tp3     | x                    | x                    | 10.3.1.195/28        | 10.3.1.296/28         |
