* # TP1 - Mise en jambes

## I. Exploration locale en solo

### 1. Affichage d'informations sur la pile TCP/IP locale
#### En ligne de commande
* Affichez les infos des cartes réseau de votre PC:
 `ipconfig`
 ```
 PS C:\Users\kevin> ipconfig

Configuration IP de Windows


Carte Ethernet Ethernet :

   Statut du média. . . . . . . . . . . . : Média déconnecté
   Suffixe DNS propre à la connexion. . . : 
   
Carte réseau sans fil Wi-Fi :

   Suffixe DNS propre à la connexion. . . : auvence.co
   Adresse IPv6 de liaison locale. . . . .: fe80::cc9f:e6bf:8e24:cfe2%14
   Adresse IPv4. . . . . . . . . . . . . .: 10.33.1.93
   Masque de sous-réseau. . . . . . . . . : 255.255.252.0
   Passerelle par défaut. . . . . . . . . : 10.33.3.253 
 ```
 
* Affichez votre gateway
`ipconfig`
`Passerelle par défaut. . . . . . . . . : 10.33.3.253`

#### En graphique (GUI : Graphical User Interface)

Panneau de configuration > Réseau et Internet > Centre Réseau et partage : WI-FI : Détails

![](./img/CR.png)


Question : 
La gateway d'Ynov permet de relier le réseau local au réseau internet

### 2. Modifications des informations

#### A. Modification d'adresse IP (part 1)

Panneau de configuration\Réseau et Internet\Centre Réseau et partage > Modifier les paramètres de la carte > double clic sur la carte réseau > propriétés > double clic sur 'Protocole Internet version 4 (TCP/IPv4)

![](./img/IP1.png)

#### B. Table ARP

* Exploration de la table ARP : 
Affichage de la table ARP grâce à la commande `arp -a`
```
PS C:\Users\kevin> arp -a

Interface : 10.33.1.93 --- 0xe
  Adresse Internet      Adresse physique      Type
  10.33.0.84            98-af-65-d7-82-f0     dynamique
  10.33.0.220           3c-9c-0f-ca-1f-57     dynamique
  10.33.2.97            d8-12-65-b5-11-77     dynamique
  10.33.3.220           40-ec-99-77-ab-82     dynamique
  10.33.3.253           00-12-00-40-4c-bf     dynamique
  10.33.3.255           ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
  255.255.255.255       ff-ff-ff-ff-ff-ff     statique

Interface : 192.168.127.1 --- 0x14
  Adresse Internet      Adresse physique      Type
  192.168.127.255       ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique

Interface : 192.168.236.1 --- 0x15
  Adresse Internet      Adresse physique      Type
  192.168.236.255       ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
```
Adresse de la passerelle
`10.33.3.253           00-12-00-40-4c-bf     dynamique`
On repère l'adresse Mac de la passerelle en utilisant l'adresse IP de la passerelle, puis on regarde l'adresse max associée à cette adresse IP


```
PS C:\Users\kevin> ping 10.33.3.109
PS C:\Users\kevin> ping 10.33.3.112
PS C:\Users\kevin> ping 10.33.3.130
PS C:\Users\kevin> arp -a

Interface : 10.33.1.93 --- 0xe
  Adresse Internet      Adresse physique      Type
  10.33.0.84            98-af-65-d7-82-f0     dynamique
  10.33.0.220           3c-9c-0f-ca-1f-57     dynamique
  10.33.2.93            d8-12-65-b5-11-77     dynamique
  10.33.2.97            d8-12-65-b5-11-77     dynamique
  10.33.2.105           ec-2e-98-ca-da-e9     dynamique
  10.33.3.109           38-f9-d3-2f-c2-79     dynamique
  10.33.3.112           3c-06-30-2d-48-0d     dynamique
  10.33.3.130           18-1d-ea-b4-8c-82     dynamique
  10.33.3.220           40-ec-99-77-ab-82     dynamique
  10.33.3.253           00-12-00-40-4c-bf     dynamique
  10.33.3.255           ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
  255.255.255.255       ff-ff-ff-ff-ff-ff     statique

Interface : 192.168.127.1 --- 0x14
  Adresse Internet      Adresse physique      Type
  192.168.127.255       ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique

Interface : 192.168.236.1 --- 0x15
  Adresse Internet      Adresse physique      Type
  192.168.236.255       ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
```

10.33.3.109 -> 38-f9-d3-2f-c2-79
10.33.3.112 -> 3c-06-30-2d-48-0d
10.33.3.130 -> 18-1d-ea-b4-8c-82

#### C. nmap

Scan ping du réseau grace à la commande `nmap -sP 10.33.0.0/22`
```
PS C:\Users\kevin> nmap -sP 10.33.0.0/22
Starting Nmap 7.92 ( https://nmap.org ) at 2021-09-16 13:57 Paris, Madrid (heure dÆÚtÚ)
Nmap scan report for 10.33.0.3
Host is up (12s latency).
MAC Address: 22:93:D2:7E:9F:1A (Unknown)
Nmap scan report for 10.33.0.8
Host is up (0.34s latency).
MAC Address: EE:6B:A1:17:E3:B6 (Unknown)
Nmap scan report for 10.33.0.20
Host is up (0.034s latency).
MAC Address: 74:29:AF:33:1B:69 (Hon Hai Precision Ind.)
Nmap scan report for 10.33.0.21
Host is up (0.0050s latency).
MAC Address: B0:FC:36:CE:9C:89 (CyberTAN Technology)
Nmap scan report for 10.33.0.27
Host is up (0.0060s latency).
MAC Address: A8:64:F1:8B:1D:4D (Intel Corporate)
Nmap scan report for 10.33.0.31
Host is up (0.87s latency).
MAC Address: FA:C5:6D:60:4B:4C (Unknown)
Nmap scan report for 10.33.0.38
Host is up (0.0070s latency).
MAC Address: 84:1B:77:F9:21:71 (Intel Corporate)
Nmap scan report for 10.33.0.41
Host is up (0.088s latency).
MAC Address: B0:EB:57:82:54:35 (Huawei Technologies)
Nmap scan report for 10.33.0.45
```
Affichage de la table ARP :
```
PS C:\Users\kevin> arp -a

Interface : 192.168.176.1 --- 0xa
  Adresse Internet      Adresse physique      Type
  192.168.176.255       ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique

Interface : 10.33.1.93 --- 0xf
  Adresse Internet      Adresse physique      Type
  10.33.0.59            e4-0e-ee-73-73-96     dynamique
  10.33.0.61            14-85-7f-fe-94-1c     dynamique
  10.33.0.71            f0-03-8c-35-fe-47     dynamique
  10.33.0.75            62-77-19-80-6e-28     dynamique
  10.33.0.152           ea-7e-bb-50-99-95     dynamique
  10.33.0.195           12-b9-05-11-2c-54     dynamique
  10.33.0.207           c0-3c-59-a9-bd-d9     dynamique
  10.33.1.39            70-66-55-dd-96-4f     dynamique
  10.33.1.89            ac-67-5d-00-06-45     dynamique
  10.33.1.125           08-f8-bc-6c-b6-02     dynamique
  10.33.1.140           fa-38-8f-3f-42-a0     dynamique
  10.33.1.194           20-16-b9-84-86-1d     dynamique
  10.33.1.247           08-71-90-c7-c7-7a     dynamique
  10.33.2.3             e0-2b-e9-42-5e-91     dynamique
  10.33.2.14            84-fd-d1-10-23-45     dynamique
  10.33.2.21            f6-53-a2-fe-e7-15     dynamique
  10.33.2.41            48-a4-72-41-c6-d4     dynamique
  10.33.2.42            16-d8-79-91-57-a4     dynamique
  10.33.2.61            88-66-5a-51-c5-89     dynamique
  10.33.2.92            14-7d-da-4a-1d-27     dynamique
  10.33.2.93            d8-12-65-b5-11-77     dynamique
  10.33.2.131           f8-5e-a0-04-cc-75     dynamique
  10.33.2.160           de-e6-48-af-e5-d2     dynamique
  10.33.2.193           e0-2b-e9-7d-a6-c2     dynamique
  10.33.3.12            de-2b-3c-0a-c2-43     dynamique
  10.33.3.13            26-7b-3f-46-6d-9e     dynamique
  10.33.3.30            98-01-a7-cd-a5-d3     dynamique
  10.33.3.53            ae-d7-ca-4a-d6-8c     dynamique
  10.33.3.67            dc-41-a9-58-d0-c7     dynamique
  10.33.3.155           88-66-5a-2a-28-70     dynamique
  10.33.3.179           94-e7-0b-0a-46-aa     dynamique
  10.33.3.226           f0-77-c3-06-8f-42     dynamique
  10.33.3.229           94-08-53-46-75-d3     dynamique
  10.33.3.243           74-40-bb-2f-2b-dd     dynamique
  10.33.3.253           00-12-00-40-4c-bf     dynamique
  10.33.3.255           ff-ff-ff-ff-ff-ff     statique
  224.0.0.2             01-00-5e-00-00-02     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
  255.255.255.255       ff-ff-ff-ff-ff-ff     statique

Interface : 192.168.127.1 --- 0x15
  Adresse Internet      Adresse physique      Type
  192.168.127.255       ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique

Interface : 192.168.236.1 --- 0x16
  Adresse Internet      Adresse physique      Type
  192.168.236.255       ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique

Interface : 10.10.1.1 --- 0x19
  Adresse Internet      Adresse physique      Type
  10.10.1.255           ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
  255.255.255.255       ff-ff-ff-ff-ff-ff     statique
```
#### D. Modification d'adresse IP
On modifie l'adresse ip et la gateway dans les paramètres de la carte réseau
![](./img/IP2.png)


```
PS C:\Users\kevin> nmap -sP 10.33.0.0/22
Starting Nmap 7.92 ( https://nmap.org ) at 2021-09-16 14:06 Paris, Madrid (heure dÆÚtÚ)
Nmap scan report for 10.33.0.3
Host is up (0.28s latency).
MAC Address: 22:93:D2:7E:9F:1A (Unknown)
Nmap scan report for 10.33.0.8
[...]
Nmap done: 1024 IP addresses (180 hosts up) scanned in 70.22 seconds
```

```PS C:\Users\kevin> ipconfig
[...]
Carte réseau sans fil Wi-Fi :

   Suffixe DNS propre à la connexion. . . : auvence.co
   Adresse IPv6 de liaison locale. . . . .: fe80::cc9f:e6bf:8e24:cfe2%15
   Adresse IPv4. . . . . . . . . . . . . .: 10.33.1.85
   Masque de sous-réseau. . . . . . . . . : 255.255.252.0
   Passerelle par défaut. . . . . . . . . : 10.33.3.253
[...]
```

On vérifie la connexion à internet en faisant un ping vers le serveur DNS de google `ping 8.8.8.8`

```
PS C:\Users\kevin> ping 8.8.8.8

Envoi d’une requête 'Ping'  8.8.8.8 avec 32 octets de données :
Réponse de 8.8.8.8 : octets=32 temps=20 ms TTL=114
Réponse de 8.8.8.8 : octets=32 temps=59 ms TTL=114
Réponse de 8.8.8.8 : octets=32 temps=20 ms TTL=114
Réponse de 8.8.8.8 : octets=32 temps=19 ms TTL=114

Statistiques Ping pour 8.8.8.8:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 19ms, Maximum = 59ms, Moyenne = 29ms
```
```
PS C:\Users\kevin> ping 10.10.1.10

Envoi d’une requête 'Ping'  10.10.1.10 avec 32 octets de données :
Réponse de 10.10.1.1 : Impossible de joindre l’hôte de destination.
Réponse de 10.10.1.10 : octets=32 temps<1ms TTL=128
Réponse de 10.10.1.10 : octets=32 temps=2 ms TTL=128
Réponse de 10.10.1.10 : octets=32 temps=2 ms TTL=128
```
## Exploration locale en duo

### 3 Modification d'adresse IP

On modifie l'adresse IP dans les paramètres de la carte Windows, pour cette partie mon PC aura l'adresse IP 192.168.69.2 et l'autre PC l'adresse 192.168.69.2
```
PS C:\Users\kevin> ipconfig

Configuration IP de Windows


Carte Ethernet Ethernet :

   Suffixe DNS propre à la connexion. . . :
   Adresse IPv6 de liaison locale. . . . .: fe80::18ef:129c:6a3f:15cc%19
   Adresse IPv4. . . . . . . . . . . . . .: 192.168.69.2
   Masque de sous-réseau. . . . . . . . . : 255.255.255.252
   Passerelle par défaut. . . . . . . . . : 
```

On ping l'autre machine du réseau
```
PS C:\Users\kevin> ping 192.168.69.1

Envoi d’une requête 'Ping'  192.168.69.1 avec 32 octets de données :
Réponse de 192.168.69.1 : octets=32 temps=4 ms TTL=128
Réponse de 192.168.69.1 : octets=32 temps=2 ms TTL=128
Réponse de 192.168.69.1 : octets=32 temps=2 ms TTL=128
Réponse de 192.168.69.1 : octets=32 temps=2 ms TTL=128

Statistiques Ping pour 192.168.69.1:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 2ms, Maximum = 4ms, Moyenne = 2ms
```

On affiche ensuite la table arp `arp -a`:
```
[...]
Interface : 192.168.69.2 --- 0x13
  Adresse Internet      Adresse physique      Type
  10.10.1.10            d4-5d-64-62-19-14     dynamique
  192.168.69.1          d4-5d-64-62-19-14     dynamique
  192.168.69.3          ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
[...]
```

### Utilisation d'un des deux en gateway

Pc Non connecté : 


ipconfig : 
```
Carte Ethernet Ethernet :

   Suffixe DNS propre à la connexion. . . :
   Adresse IPv6 de liaison locale. . . . .: fe80::18ef:129c:6a3f:15cc%19
   Adresse IPv4. . . . . . . . . . . . . .: 192.168.69.2
   Masque de sous-réseau. . . . . . . . . : 255.255.255.252
   Passerelle par défaut. . . . . . . . . : 192.168.69.1
    [...]
    Carte réseau sans fil Wi-Fi :

   Statut du média. . . . . . . . . . . . : Média déconnecté
```

On ping le dns de google pour savoir si on est connecté à internet puis on effectue un tracert
```
PS C:\Users\kevin> ping 8.8.8.8

Envoi d’une requête 'Ping'  8.8.8.8 avec 32 octets de données :
Réponse de 8.8.8.8 : octets=32 temps=20 ms TTL=114
Réponse de 8.8.8.8 : octets=32 temps=21 ms TTL=114
Réponse de 8.8.8.8 : octets=32 temps=21 ms TTL=114
Réponse de 8.8.8.8 : octets=32 temps=20 ms TTL=114

Statistiques Ping pour 8.8.8.8:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 20ms, Maximum = 21ms, Moyenne = 20ms
PS C:\Users\kevin> tracert google.com

Détermination de l’itinéraire vers google.com [172.217.22.142]
avec un maximum de 30 sauts :

  1     2 ms     1 ms     2 ms  DESKTOP-KHB2MRJ [192.168.69.1]
  2     *        *        *     Délai d’attente de la demande dépassé.
  3    14 ms     9 ms     4 ms  10.33.3.253
  4     5 ms    11 ms     4 ms
PS C:\Users\kevin>
```

PC connecté : 
Panneau de configuration\Réseau et Internet\Centre Réseau et partage > modifier les paramètres de la carte > double clique sur la carte réseau connecté à internet > propriétés > on clique sur l'onglet partage puis on coche "autoriser d'autres utilisateurs du réseau à se connecter via la connexion internet de cet ordinateur puis on indique la carte Ethernet où l'on souhaite effectuer le partage"

### 5 Petit chat privé 

**Client :**

On utilise la commande `./nc.exe 192.168.69.1 8888` ,en ayant ouvert powershell dans le dossier contenant les fichiers netcat, pour se connecter au serveur présent sur l'autre machine
```
PS C:\Users\kevin\Desktop\netcat-1.11> ./nc.exe 192.168.69.1 8888
test
test

Je réponds au test !
ahah trop bien
:)
```

**Serveur :**

On utilise la commande`./nc.exe -l -p 8888` afin que le serveur écoute un port  qui permet à l'autre machine de se connecter
```
PS C:\Users\kevin\Desktop\netcat-1.11> ./nc.exe -l -p 8888
Bonjour ! x)
Yo
Wow ! Superbe ce chat !
trop bien
```
#### Pour aller plus loin

On précise l'adresse Ip que l'on souhaite écouter en plus du port `./nc.exe -l -p 8888 192.168.69.1`
```
PS C:\Users\kevin\Desktop\netcat-1.11> ./nc.exe -l -p 8888 192.168.69.1
test
WOOOOOOW SUPER !!!!!!!!!!!!!!!!!!!
```
### Firewall
Règles du firewall:

![](./img/Capture.png)
Ping avec firewall
```
PS C:\Users\kevin\Desktop\netcat-1.11> ping 192.168.69.1

Envoi d’une requête 'Ping'  192.168.69.1 avec 32 octets de données :
Réponse de 192.168.69.1 : octets=32 temps=2 ms TTL=128
Réponse de 192.168.69.1 : octets=32 temps=2 ms TTL=128
Réponse de 192.168.69.1 : octets=32 temps=2 ms TTL=128
Réponse de 192.168.69.1 : octets=32 temps=2 ms TTL=128

Statistiques Ping pour 192.168.69.1:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 2ms, Maximum = 2ms, Moyenne = 2ms
```
Netcat avec firewall
```
PS C:\Users\kevin\Desktop\netcat-1.11> ./nc.exe 192.168.69.1 1337
Test netcaat !
ouais
lourd tout ça
trop bien
```

## III Manipulations d'autres outils/protocoles côté client
### 1. DHCP
Panneau de conifg > cartes réseau > double clic sur la carte > Détails
On obtient cela
Bail expirant: jeudi 16 septembre 2021 18:09:44
Serveur DHCP IPv4: 10.33.3.254
```
Suffixe DNS propre à la connexion: auvence.co
Description: Intel(R) Wireless-AC 9560 160MHz
Adresse physique: ‎B8-9A-2A-3D-C1-1A
DHCP activé: Oui
Adresse IPv4: 10.33.1.93
Masque de sous-réseau IPv4: 255.255.252.0
Bail obtenu: jeudi 16 septembre 2021 15:31:02
Bail expirant: jeudi 16 septembre 2021 18:09:44
Passerelle par défaut IPv4: 10.33.3.253
Serveur DHCP IPv4: 10.33.3.254
Serveurs DNS IPv4: 10.33.10.2, 10.33.10.148, 10.33.10.155
Serveur WINS IPv4: 
NetBIOS sur TCP/IP activé: Oui
Adresse IPv6 locale de lien: fe80::cc9f:e6bf:8e24:cfe2%15
Passerelle par défaut IPv6: 
Serveur DNS IPv6: 
```

### 2. DNS
En utilisant la même manipulation pour trouver l'adresse du serveur dhcp on obtient la liste des serveurs DNS
`Serveurs DNS IPv4: 10.33.10.2, 10.33.10.148, 10.33.10.155`

#### NSLookup

```
PS C:\Users\kevin\Desktop\netcat-1.11> nslookup google.com
Serveur :   dns.google
Address:  8.8.8.8

Réponse ne faisant pas autorité :
Nom :    google.com
Addresses:  2a00:1450:4007:817::200e
          216.58.201.238

PS C:\Users\kevin\Desktop\netcat-1.11> nslookup ynov.com
Serveur :   dns.google
Address:  8.8.8.8

Réponse ne faisant pas autorité :
Nom :    ynov.com
Address:  92.243.16.143
```

La commande nslookup permet de faire le lien entre un nom de domaine et son adresse IP, l'adresse IP de google.com est donc 8.8.8.8. On peut aussi voir que les requêtes ont été effectuées sur le serveur dns de google sur l'adresse 8.8.8.8

```
PS C:\Users\kevin\Desktop\netcat-1.11> nslookup 78.74.21.21
Serveur :   dns.google
Address:  8.8.8.8

Nom :    host-78-74-21-21.homerun.telia.com
Address:  78.74.21.21

PS C:\Users\kevin\Desktop\netcat-1.11> nslookup 92.146.54.88
Serveur :   dns.google
Address:  8.8.8.8

Nom :    apoitiers-654-1-167-88.w92-146.abo.wanadoo.fr
Address:  92.146.54.88
```
La commande nslookup permet aussi de fiare la recherche dans l'autre sens, en utilisant des adresses IP dans la commande on obtient donc le nom de domaine de ces adresses

## Wireshark
**Ping :**

Trames des ping vers la passerelles d'ynov
![](./img/ping.png)

**Netcat :**

```
PS C:\Users\kevin\Desktop\netcat-1.11> ./nc.exe 192.168.69.1 1337
test
trop cool de voir les messages dans les trames
```
Trames du chat netcat (on peut voir les messages dans les trames)
![](./img/trames.png)

**DNS:**

Trames obtenus en ayant utiliser la commande `nslookup 8.8.8.8`
![](./img/DNS.png)

